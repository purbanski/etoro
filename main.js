function Beeper() {

	this.audioCtx = new AudioContext();

	this.config = {
		'gain' : {
			'min' : 0,
			'max' : 30
		},
		'frequency' : { 
			'min' : 150,
			'max' : 1300
		},
		'len' : 0.2
	}
}

Beeper.prototype.beep = function(gain) {

	var config = this.config;
	this.beep_(gain, config.gain.min, config.gain.max, config.frequency.min, config.frequency.max);
}

Beeper.prototype.beep_ = function(gain, minGain, maxGain, minFreq, maxFreq) {

	gain = gain > minGain ? gain : minGain;
	gain = gain < maxGain ? gain : maxGain;
	
	let freq = minFreq + gain * ((maxFreq - minFreq)/(maxGain - minGain));
	freq = freq > minFreq ? freq : minFreq;
	freq = freq < maxFreq ? freq : maxFreq;


	let o = this.audioCtx.createOscillator();
	let len = this.config.len;

	o.frequency.value = freq;
	
	//setTimeout(() => o.frequency.value *= Math.pow(2, 1/12), 1000);
	//setTimeout(() => o.frequency.value *= Math.pow(2, 1/12), 2000);

	o.connect(this.audioCtx.destination);
	o.start();
	o.stop(this.audioCtx.currentTime + len);

}

function Gain() {

	var get_gain_arr = function() {
		var gain_col = $("[data-etoro-automation-id='portfolio-manual-trades-table-body-profit-value']");

		if (typeof gain_col !== 'object' || gain_col === null ) {
		//	new Error("Can't find gain data column');
			console.warn("Can't find gain data column");
			return [];
		}
		var gain_arr = [];

		$.each(gain_col, function(k,v) { 
			var gain_txt = v.innerText;
			gain_txt = gain_txt.split('$').join('');
	   
			var gain_float = parseFloat(gain_txt);
	   	gain_arr.push(gain_float);
		});

		return gain_arr;
	}


	function _Gain() {
		this.beeper = new Beeper();
		this.intervalId = 0;
	}

	_Gain.prototype.check = function(expected_gain, gain_arr) {

		console.log("Gain watcher is running");

		if (typeof gain_arr === 'undefined') {
			gain_arr = get_gain_arr();
		}
		
		var snd_playing = false;
		var max = 0;

		gain_arr.forEach(function(g) {
			if ( g >= expected_gain ) {
				max = max > g ? max : g;				
			}
		});

		if ( max > 0 && this.sound ) {
			this.beeper.beep(max);
		}
	}

	_Gain.prototype.start = function(expected_gain, gain_arr) {

		expected_gain = expected_gain || 0;

		console.log("Gain watchers has been started");

		var self = this;
		this.intervalId = setInterval(function() {
			self.check(expected_gain, gain_arr);
		}, 1000);
	}

	_Gain.prototype.stop = function() {
		if (this.intervalId !== 0) {
			clearInterval(this.intervalId);
			this.intervalId = 0;
		}
	}

	_Gain.prototype.sound = true;

	return new _Gain();
}


var g = new Gain();

setTimeout(function() {
    g.start();
    
}, 5500);
